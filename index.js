/**
 * input: 
 * - diem chuan cua hoi dong 
 * - nhap vao diem 3 mon thi cua thi sinh
 * - khu vuc (X neu khong thuoc khu vuc nao)
 * - doi tuong uu tien (nhap X neu khong phai la doi tuong uu tien)
 * progress: lay value user nhap
 * b1: ktr value == 0 ? rot : tt ktr dkien khac
 * b2- so tinh tong diem cua sv = tong 3 mon + doi tuong && khu vuc (neu co);
 * b3: so sanh diem voi diem cua hoi dong ==> sinh vien dau hay rot
 * output: cho biet thi xinh do dau hay rot
 */

document.getElementById('btn-1').onclick =function() {
    var hoiDong = document.getElementById('hoiDong').value*1;
    var khuVuc =  document.getElementById('khuVuc').value;
    var doiTuong = document.getElementById('doiTuong').value;
    var diem1 = document.getElementById('diem1').value*1;
    var diem2 = document.getElementById('diem2').value*1;
    var diem3 = document.getElementById('diem3').value*1;
    if (diem1 <= 0 || diem2 <= 0 || diem3 <= 0) {
     document.getElementById('ketQua1').innerHTML =  `Bạn đã rớt tốt nghiệp vì một hoặc nhiều môn số điểm của bạn = 0`
    }
 var diemKhuVuc = khuVuc == 'A' ? 2 : khuVuc == 'B' ? 1 : khuVuc == 'C' ? 0.5 :  0
 var diemDoiTuong = doiTuong === 1 ? 2.5 : doiTuong == 2 ? 1.5 : doiTuong == 3 ? 1 : 0
 var diemTongSV = diem1 + diem2 + diem3 + diemKhuVuc + diemDoiTuong;
    if(diemTongSV >= hoiDong) {
     document.getElementById('ketQua1').innerHTML = `Chúc mừng bạn đã đậu với số điểm ${diemTongSV}`
    } else {
     document.getElementById('ketQua1').innerHTML = `Rất tiêc! Bạn chưa đủ điều kiện`
    }
 
 }

 /**
  * Bài 2:Viêt chương trình tính tiền điên
  * input: value người dùng nhập vào
  * progress: ss value >0
  *         50kw : 500d
  *         51-100: 650d
  *         51-100: 850d
  *         101-150: 1100d
  *         >150: 1300d
  * ouput : Xuất ra só tiền phải đóng
  */

 document.getElementById('bai-2').onclick = function() {
    var suDung = document.getElementById('suDung').value*1;
    var phaiTra;
    if(suDung <=0) {
        alert('Vui lòng nhập số điện sử dụng');
    } else {
        if (suDung <= 50) {
            phaiTra = suDung * 500
        } else if  (suDung <= 100) {
            phaiTra = (50*500) + (suDung-50) *650
        } else if  (suDung <= 200) {
            phaiTra = (50*500) + (50*650) + (suDung-100) *850
        } else if  (suDung <= 350) {
            phaiTra = (50*500) + (50*650) + (100*850) + (suDung-200) *1100
        } else {
            phaiTra = (50*500) + (50*650) + (100*850) + (150*1100) + (suDung-350) *1300
        }
    }
    document.getElementById('ketQua2').innerHTML = `Tiền điện của bạn là: ${phaiTra.toLocaleString()} kw`
}
 
// bài 3:
/**
 * input: 
 *  - Họ Tên:
 *  - Tổng thu nhập năm
 *  - Số người phụ thuộc
 * output: Xuất ra số tiền thuế
 * progress: lấy value của user.
 * value <= 60 ? 5% : value <= 120 ? 10% : value = 210 ? 15% : value <= 384 ? 20% : value <= 624 ? 25% : value <= 960 ? 30% : 35%
 */

document.getElementById('btn-3').onclick = function() {
    var hoTen = document.getElementById('hoTen').value;
    var thuNhap = document.getElementById('thuNhap').value*1;
    var soNguoi = document.getElementById('soNguoi').value*1;
    var thuNhapChiuThue = thuNhap - 4 - (soNguoi*1.6);
    var dongThue;
    if(hoTen === '' || thuNhap === '' ) {
        alert('Vui lòng nhập đẩy đủ thông tin')
    } 
    // if(thuNhapChiuThue <= 60) {
    //     dongThue = thuNhapChiuThue * 0.05;
    // } else if
    dongThue = thuNhapChiuThue <= 60 ? thuNhapChiuThue * 0.05 : thuNhapChiuThue <= 120 ? thuNhapChiuThue * 0.1 : thuNhapChiuThue <= 210 ? thuNhapChiuThue * 0.15 : thuNhapChiuThue <= 384 ? thuNhapChiuThue * 0.2 :  thuNhapChiuThue <= 624 ? thuNhapChiuThue * 0.25 :  thuNhapChiuThue <= 960 ? thuNhapChiuThue * 0.30 : thuNhapChiuThue * 0.35;
    document.getElementById('ketQua3').innerHTML = `Số tiền thuế của bạn là: ${dongThue.toLocaleString()} Triệu`
}

/**
 * bài 4:  Bài 4:
 * input:  
 *  - Mã khách hàng
 *  - Loại khách hàng
 *  - so ket noi
 *  - so kenh cao cap
 * output: In ra số tiền Cáp
 * progress:
 * - lấy lựa chọn của user (if== nhà dân ==> ẩn số kn) else ==> hiện số kết nối
 *  if user == nhaDan => soTien = phí sử lý hóa đơn + Phí dịch vụ cơ bản + thuê kênh cao cấp * 7.5
 * else soTien == phi sử lí hóa đơn + phi dic vu co ban (if <= 10 ==> phi dich vu co ban == soketnoi *75) (else phi dich vu co ban === 10*75 + soketnoi-10)
 */
const ketNoi = document.querySelector('.so-ket-noi')
ketNoi.style.display = 'none';
var khachHang = document.getElementById('khachHang')
khachHang.onchange = function() {
    khachHang.value;
    if (khachHang.value == 'doanhNghiep') {
        ketNoi.style.display = 'block';
    } else {
        ketNoi.style.display = 'none';

    }
}

document.getElementById('btn-4').onclick = function() {
    var soKetNoi = document.getElementById('soKetNoi').value*1
    var ketNoiDau = 7.5;
    var kenhCaoCap = document.getElementById('kenhCaoCap').value*1;
    var giaTienTrenKenh;
    var Tong;
    var tong_10ketNoiDau = 75;
    var tienKetNoi;
    if (khachHang.value == 'doanhNghiep') {
        if (soKetNoi<10) {
            tienKetNoi = soKetNoi * ketNoiDau
            giaTienTrenKenh = kenhCaoCap * 50 
            Tong = 15 + tienKetNoi + giaTienTrenKenh
        } else {
            for (var i = 1; i <= (soKetNoi-10); i++) {
            tong_10ketNoiDau +=5;
            tienKetNoi = tong_10ketNoiDau; 
            } 
        giaTienTrenKenh = kenhCaoCap * 50 
        Tong = 15 + tienKetNoi + giaTienTrenKenh
        } 
        document.getElementById('ketQua4').innerHTML = `<p>Phí Xử Lý Hóa Đơn: 15$</p>  <p>Phí Dịch Vụ Cơ Bản: ${tienKetNoi}$</p>  <p>Thuê Kênh Cao Cấp: ${giaTienTrenKenh}$</p> <p>Tổng = ${Tong}$</p>` 
    }  
    if(khachHang.value == 'nhaDan') {
            giaTienTrenKenh = kenhCaoCap * 7.5;
            Tong = giaTienTrenKenh + 4.5 + 20.5;
            document.getElementById('ketQua4').innerHTML = `<p>Phí Xử Lý Hóa Đơn: 4.5$</p>  <p>Phí Dịch Vụ Cơ Bản: 20.5$</p>  <p>Thuê Kênh Cao Cấp: ${giaTienTrenKenh}$</p> <p>Tổng = ${Tong}$</p>` 
    } 
}